(ns lab3.core
  (:require [scenari.core :refer :all])
  (:gen-class))
(require '[scenari.core :refer :all])

(exec-spec "resources/product-catalog.feature")


(defn positive-numbers
  ([] (positive-numbers 1))
  ([n] (lazy-seq (cons n (positive-numbers (inc n))))))

(defn random-numbers []
  (lazy-seq(cons (rand-int 10) (random-numbers)))
  )

(defn expected_value [s]
  (/ (reduce + s) (count s))
  )

(defn square [element]
  (* element element)
  )

(defn dispersion
  ([small_s] (
               let [expected_value_cache (expected_value small_s)]
               (
                 / (reduce +
                           (map #(square %) (map #(- % expected_value_cache) small_s)))
                   (- (count small_s) 1)
                   ))
    )

  ([full_s, width]
   (let [s (take width full_s)]
     ;(print s)
     (if (empty? full_s) ()
        (lazy-seq ( cons (dispersion s) (dispersion (drop width full_s) width)))))))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  ;(println "Hello, World!"))
  (print (take 10 (dispersion (random-numbers) 10))))