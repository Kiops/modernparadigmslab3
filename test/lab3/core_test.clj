(ns lab3.core-test
  (:require [clojure.test :refer :all]
            [lab3.core :refer :all]
            [scenari.core :refer :all])
  )

(defn parse-if-string [s]
  (if (string? s) (read-string s) s)
  )

;(deftest a-test
;  (testing "FIXME, I fail."
;    (is (= 0 1))))

(defwhen #"I put sequence of (.*) and no window"  [_ s]  (
  do (dispersion s)))

(defwhen #"I put sequence of (.*) and (.*) as window width"  [_ s window]
         (do (dispersion s (read-string window))))

(defwhen #"I put lazy-seq of positive numbers and (.*) as window width taking (.*) of dispersions" [_ window ammount]
         (do ( take (read-string ammount)
                    (dispersion (positive-numbers) (read-string window)))))

(defthen #"I receive (.*) as a dispersion of sequence" [prev-data result]
           (is (= prev-data (parse-if-string result))))

(exec-spec "test/lab3/bdd_text")